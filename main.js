const express = require('express');
const sqlite3 = require('sqlite3');

const srv = express();

srv.get('/dizoi/:nome', (req, res)=>{
    const db = new sqlite3.Database('mensagens.db'); // Conectando ao banco 'mensagens.db'
    const sql = `select nome,msg from mensagens where nome=?`; // Consultando no banco

    db.all(sql, [req.params.nome], (erro, linhas) => { // Aqui definimos parâmetros = 'nome'
        if(erro) throw erro;
        res.json(linhas);
    });
});


srv.listen(3030, ()=>{
    console.log('Iniciando...');
});