// Carrega o módulo sqlite3
const sqlite3 = require('sqlite3');
const express = require('express');

const srv = express();

srv.get('/deixaroi/:nome/:msg', (req, res)=>{
    const db = new sqlite3.Database('mensagens.db');
    const sql = `insert into mensagens values(?, ?);`;

    db.run(sql, [req.params.nome, req.params.msg], (resultado, erro)=>{
        if(erro) {
            res.json({
                status:'Erro',
                msg: erro})
        } else {
            res.json({
                status: 'OK',
                msg: 'Recado enviado.'})
        }
    });
})

srv.listen(3030, ()=>{
    console.log(`Iniciando 'testeInsert-2'`);
});
