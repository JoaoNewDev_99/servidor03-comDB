// Carrega o módulo sqlite3
const sqlite3 = require('sqlite3');

// Cria um objeto `Database` = db
let db = new sqlite3.Database('mensagens.db');
const sql = `SELECT nome,msg FROM mensagens`;
const sqlite = `SELECT nome,msg FROM mensagens WHERE nome=?`

db.all(sql, [], (erro, linhas) => { // All() consulta todas as linhas
    if(erro) throw erro;
    console.table(linhas);
});

db.each(sql, [], (erro, linha) => { // Each() consulta linha por linha
    if(erro) throw erro;
    console.log(`${linha.nome}: ${linha.msg}`);
});

db.get(sqlite, ['Gustavo'], (erro, linha) => {
    if(erro) throw erro;
    console.table(linha);
});




db.close(); // Fecha a conexão com o banco
