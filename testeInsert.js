// Carrega o módulo sqlite3
const sqlite3 = require('sqlite3');

// Cria um objeto `Database` = db
let db = new sqlite3.Database('mensagens.db'); // Conecta ao banco 'mensagens.db'
const sql = `insert into mensagens values(?, ?);`
/*const sqlite = `SELECT nome,msg FROM mensagens WHERE nome=?`*/

db.run(sql, ['Martina','Oi, Martina!']);
db.run(sql, ['teste','Insert ao rodar o script.']);

db.close(); // Fecha a conexão com o banco
